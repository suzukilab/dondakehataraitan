﻿Imports System.Management
Imports System.Data.SQLite
Imports System.Text
Imports System.IO

Public Class Main

    '[sqlite]
    Dim DbCon As New SQLiteConnection
    Dim DbCmd As SQLiteCommand
    Dim DataReader As SQLiteDataReader
    Dim g_pcname_exists As Boolean = False

    'イベントログID
    Const EVENTID_BOOT = 12
    Const EVENTID_SHUTDOWN = 13

    Private Sub Form_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Dim dtsRtn As DataSet
        Dim intRtn As Integer = 0
        Dim i As Integer

        'Dim formSettings As New Settings()
        'formSettings.ShowDialog()

        'グリッドビュー非表示
        dgvDisplay.Visible = False

        '[sqlite] Connection Strings
        DbCon.ConnectionString = "Version=3;Data Source=DondakeHataraitan.abc;New=False;Compress=True;"
        '[sqlite] Open
        DbCon.Open()

        i = 0

        'M_PCがrecord0件の場合、PCNAMEを取得する
        intRtn = selectCountQuery("M_PC")
        If intRtn = 0 Then
            g_pcname_exists = False

            'ようこそ画面
            Dim formWelcome As New Welcome()
            formWelcome.ShowDialog()

            'MsgBox("【1/4】ようこそ！" & vbCrLf & "初期設定を行います。")

            '[TODO]コマンドプロンプト上の設定でイベントログの保存期間を伸ばす
            'http://www.atmarkit.co.jp/fwin2k/win2ktips/1194evtarchive/evtarchive.html
        Else
            g_pcname_exists = True
        End If

        ''M_WORKERがrecord0件なら、入力を促す
        'intRtn = selectCountQuery("M_WORKER")
        'If intRtn = 0 Then
        '    MsgBox("【3/4】あなた自身の情報を入力してください。")
        'End If

        ''M_OFFICEがrecord0件なら、入力を促す
        'intRtn = selectCountQuery("M_OFFICE")
        'If intRtn = 0 Then
        '    MsgBox("【4/4】会社の情報を入力してください。")
        'End If

        'M_HOLIDAY
        '休日登録フォーム


        '初期化する
        intRtn = initQuery("T_WORKING_START")
        intRtn = initQuery("T_WORKING_END")

        'イベントログを取得する
        getLog()

        '年月リストボックスを初期化→設定する
        selectYM.Items.Clear()

        'データあり年月を取得する
        dtsRtn = selectExistsYM()
        With dtsRtn.Tables(0)
            For Each Row In .Rows
                selectYM.Items.Add(.Rows(i).Item("ym"))
                i = i + 1
            Next
        End With

        '最終行（＝最新年月）を選択状態にする
        selectYM.SelectedIndex = i - 1

        '最終処理日を取得する


        '[sqlite] Close
        'DataReader.Close()
        'DbCmd.Dispose()
        DbCon.Close()
        DbCon.Dispose()

        'グリッドビュー表示
        dgvDisplay.ColumnHeadersDefaultCellStyle.Font = New Font("メイリオ", 8, FontStyle.Regular)
        dgvDisplay.RowHeadersDefaultCellStyle.Font = New Font("メイリオ", 8, FontStyle.Regular)
        dgvDisplay.DefaultCellStyle.Font = New Font("メイリオ", 8, FontStyle.Regular)

        dgvDisplay.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvDisplay.RowHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvDisplay.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        dgvDisplay.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
        dgvDisplay.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

        dgvDisplay.RowHeadersVisible = False


        dgvDisplay.Visible = True

    End Sub

    Private Sub getLog()

        '取得するイベントログ名
        Dim logName As String = "Application"
        'Dim logName As String = "System"
        'コンピュータ名（"."はローカルコンピュータ）
        Dim machineName As String = "."

        '指定したイベントログが存在しているか調べる
        If System.Diagnostics.EventLog.Exists(logName, machineName) Then
            'EventLogオブジェクトを作成する
            Dim log As New System.Diagnostics.EventLog(logName, machineName)

            '初回起動時（PC名がセットされていない場合）は、PC名を取得する
            If Not g_pcname_exists Then
                insertPc(log.Entries(0).MachineName)
                g_pcname_exists = True
            End If

            'ログエントリをすべて取得する
            For Each entry As System.Diagnostics.EventLogEntry In log.Entries
                'ログエントリのメッセージを出力する
                If logName = "System" Then
                    Select Case entry.InstanceId
                        Case 12, 13, 6005, 6006
                            insertWinLog(entry.TimeGenerated, entry.InstanceId)
                    End Select
                Else
                    Select Case entry.InstanceId
                        Case 1531, 1532 ', 4625, 17147, 9009　なくても良さそう
                            insertWinLog(entry.TimeGenerated, entry.InstanceId)
                    End Select
                End If

            Next

            '閉じる
            log.Close()
        End If

    End Sub

    Private Function insertPc(ByVal machineName As String) As Integer

        Dim sbSql As New StringBuilder
        Dim table As String = ""
        Dim retVal As Integer

        '[sqlite] SQL Strings
        DbCmd = DbCon.CreateCommand
        sbSql.Clear()
        sbSql.AppendLine("insert into M_PC                ")
        sbSql.AppendLine("               (                         ")
        sbSql.AppendLine("                 ADATE                   ")
        sbSql.AppendLine("                ,UDATE                   ")
        sbSql.AppendLine("                ,PCNAME                  ")
        sbSql.AppendLine("               )                         ")
        sbSql.AppendLine("     values                              ")
        sbSql.AppendLine("               (                         ")
        sbSql.AppendLine("                 '" & DateTime.Now & "'  ")
        sbSql.AppendLine("                ,'" & DateTime.Now & "'  ")
        sbSql.AppendLine("                ,'" & machineName & "'   ")
        sbSql.AppendLine("               )                         ")
        DbCmd.CommandText = sbSql.ToString

        '[sqlite] Execute
        retVal = DbCmd.ExecuteNonQuery()

        Return retVal

    End Function

    Private Function updateBreak(ByVal id As String, ByVal starttime As String, ByVal endtime As String) As Integer

        Dim sbSql As New StringBuilder
        Dim table As String = ""
        Dim retVal As Integer

        '[sqlite] SQL Strings
        DbCmd = DbCon.CreateCommand
        sbSql.Clear()
        sbSql.AppendLine("update M_BREAK                              ")
        sbSql.AppendLine("   set UDATE       = '" & DateTime.Now & "' ")
        sbSql.AppendLine("      ,BREAK_START = '" & starttime & "'    ")
        sbSql.AppendLine("      ,BREAK_END   = '" & endtime & "'      ")
        sbSql.AppendLine(" where ID = " & id & "                      ")
        DbCmd.CommandText = sbSql.ToString

        '[sqlite] Execute
        retVal = DbCmd.ExecuteNonQuery()

        Return retVal

    End Function

    Private Function initQuery(ByVal table As String) As Integer

        Dim sbSql As New StringBuilder
        Dim retVal As Integer

        '[sqlite] SQL Strings
        DbCmd = DbCon.CreateCommand
        sbSql.Clear()
        sbSql.AppendLine("delete from " & table & "                ")
        DbCmd.CommandText = sbSql.ToString

        '[sqlite] Execute
        retVal = DbCmd.ExecuteNonQuery()

        Return retVal

    End Function

    Private Function insertWinLog(ByVal timeGenerated As Date, ByVal eventId As Integer) As Integer

        Dim sbSql As New StringBuilder
        Dim table As String = ""
        Dim retVal As Integer
        Dim minusDataLineHour As Integer

        Select Case eventId

            Case 1531, 12, 6005, 4625
                table = "T_WORKING_START"
                minusDataLineHour = 0

            Case 1532, 13, 6006, 17147, 9009
                table = "T_WORKING_END"
                minusDataLineHour = 13

        End Select


        minusDataLineHour *= -1

        '[sqlite] SQL Strings
        DbCmd = DbCon.CreateCommand
        sbSql.Clear()
        sbSql.AppendLine("insert into " & table & "                ")
        sbSql.AppendLine("               (                         ")
        sbSql.AppendLine("                 ADATE                   ")
        sbSql.AppendLine("                ,UDATE                   ")
        sbSql.AppendLine("                ,YMD                     ")
        sbSql.AppendLine("                ,RESULT                  ")
        sbSql.AppendLine("               )                         ")
        sbSql.AppendLine("     values                              ")
        sbSql.AppendLine("               (                         ")
        sbSql.AppendLine("                 '" & DateTime.Now & "'  ")
        sbSql.AppendLine("                ,'" & DateTime.Now & "'  ")
        sbSql.AppendLine("                ,'" & timeGenerated.AddHours(minusDataLineHour).ToString("yyyy-MM-dd") & "' ")
        sbSql.AppendLine("                ,'" & timeGenerated.ToString("yyyy-MM-dd HH:mm:ss") & "' ")
        sbSql.AppendLine("               )                         ")
        DbCmd.CommandText = sbSql.ToString

        '[sqlite] Execute
        retVal = DbCmd.ExecuteNonQuery()

        Return retVal

    End Function

    Private Function selectCountQuery(ByVal table As String) As Integer

        Dim ret As New DataSet
        Dim sbSql As New StringBuilder
        Dim count As Integer

        '[sqlite] SQL Strings
        DbCmd = DbCon.CreateCommand

        sbSql.AppendLine("select count(1) as cnt      ")
        sbSql.AppendLine("  from " & table & "        ")

        DbCmd.CommandText = sbSql.ToString

        Dim da As New SQLiteDataAdapter
        da.SelectCommand = DbCmd
        da.Fill(ret)

        count = CInt(ret.Tables(0).Rows(0).Item("cnt"))

        Return count

    End Function

    Private Function selectExistsYM() As DataSet

        Dim ret As New DataSet
        Dim sbSql As New StringBuilder

        '[sqlite] SQL Strings
        DbCmd = DbCon.CreateCommand


        sbSql.AppendLine("   select distinct strftime('%Y年%m月', a.ymd) as ym       ")
        sbSql.AppendLine("     from T_WORKING_START                      as a        ")
        sbSql.AppendLine(" order by ym                                               ")

        DbCmd.CommandText = sbSql.ToString
        Dim da As New SQLiteDataAdapter
        da.SelectCommand = DbCmd
        da.Fill(ret)

        Return ret

    End Function

    Private Function selectWorktimeToDisplay(ByVal year As String, ByVal month As String) As DataSet

        Dim ret As New DataSet
        Dim sbSql As New StringBuilder
        Dim queryYMD As String

        '選択した年月の初日
        queryYMD = year.Trim & "-" & month.Trim & "-01"

        '[sqlite] SQL Strings
        DbCmd = DbCon.CreateCommand

        '選択した年月×ログデータのある日付データでleft join
        sbSql.AppendLine("   select a.seqdate     as seqdate                ")
        sbSql.AppendLine("         ,c.week_name   as week_name              ")
        sbSql.AppendLine("         ,b.start       as start                  ")
        sbSql.AppendLine("         ,b.end         as end                    ")
        sbSql.AppendLine("         ,b.working_time         as working_time  ")
        sbSql.AppendLine("     from (                                       ")
        sbSql.AppendLine("          select date( '" & queryYMD & "', '+' || num || ' day' ) as seqdate    ")
        sbSql.AppendLine("            from seq100                                                         ")
        sbSql.AppendLine("           where num < cast( strftime('%d' , '" & queryYMD & "', '+1 month','-1 day') as integer )   ")
        sbSql.AppendLine("          )                  as a                                ")
        sbSql.AppendLine("                                                              ")
        sbSql.AppendLine("          left join                                           ")
        sbSql.AppendLine("             V_WORKINGTIME   as b                             ")
        sbSql.AppendLine("          on a.seqdate = b.ymd                                ")
        sbSql.AppendLine("                                                              ")
        sbSql.AppendLine("          left join                                           ")
        sbSql.AppendLine("             M_WEEK          as c                             ")
        sbSql.AppendLine("          on strftime('%w', a.seqdate) = c.week_no            ")
        sbSql.AppendLine(" order by a.seqdate                                           ")

        DbCmd.CommandText = sbSql.ToString
        Dim da As New SQLiteDataAdapter
        da.SelectCommand = DbCmd
        da.Fill(ret)

        Return ret

    End Function

    Private Sub selectYM_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles selectYM.SelectedIndexChanged

        Dim year As integer
        Dim month As String
        Dim dtsRtn As DataSet
        Dim dttDisplayTable As DataTable
        Dim dtRow As DataRow
        Dim i As Integer
        Dim totalWorkingTime As TimeSpan
        Dim totalRestTime As TimeSpan
        Dim totalSumTime As TimeSpan
        Dim workingTime As String
        Dim restTime As String

        year = Mid(selectYM.SelectedItem.ToString, 1, 4)
        month = Mid(selectYM.SelectedItem.ToString, 6, 2)

        '表示データを取得する
        dtsRtn = selectWorktimeToDisplay(year, month)

        'データテーブル設定
        dttDisplayTable = dtsRtn.Tables.Add("Main")
        dttDisplayTable.Columns.Add("日付", Type.GetType("System.String"))
        dttDisplayTable.Columns.Add("出社時刻", Type.GetType("System.String"))
        dttDisplayTable.Columns.Add("退社時刻", Type.GetType("System.String"))

        dttDisplayTable.Columns.Add("経過時間", Type.GetType("System.String")) '自動計算
        dttDisplayTable.Columns.Add("控除時間", Type.GetType("System.String")) '自動計算
        'dttDisplayTable.Columns.Add("調整時間", Type.GetType("System.String")) '入力
        'dttDisplayTable.Columns.Add("実働時間", Type.GetType("System.String")) '自動計算

        'dttDisplayTable.Columns.Add("時間外労働", Type.GetType("System.String")) '自動計算
        'dttDisplayTable.Columns.Add("休日労働", Type.GetType("System.String"))
        'dttDisplayTable.Columns.Add("備考", Type.GetType("System.String"))

        'データの追加
        i = 0
        With dtsRtn.Tables(0)
            For Each Row In .Rows
                dtRow = dttDisplayTable.NewRow()

                dtRow("日付") = .Rows(i).Item("seqdate") & .Rows(i).Item("week_name")
                dtRow("出社時刻") = .Rows(i).Item("start")
                dtRow("退社時刻") = .Rows(i).Item("end")

                If Not IsDBNull(.Rows(i).Item("working_time")) Then

                    workingTime = .Rows(i).Item("working_time")
                    dtRow("経過時間") = workingTime
                    totalWorkingTime = totalWorkingTime.Add(TimeSpan.Parse(workingTime))

                    Select Case workingTime
                        Case "00:00" To "06:00"
                            restTime = "00:00"
                        Case "06:01" To "08:00"
                            restTime = "00:45"
                        Case Else
                            restTime = "01:00"
                    End Select

                    dtRow("控除時間") = restTime
                    totalRestTime = totalRestTime.Add(TimeSpan.Parse(restTime))

                End If

                dttDisplayTable.Rows.Add(dtRow)
                i = i + 1
            Next

            '最終行
            dtRow = dttDisplayTable.NewRow()
            dtRow("日付") = "小計"
            dtRow("出社時刻") = ""
            dtRow("退社時刻") = ""
            dtRow("経過時間") = Math.Floor(totalWorkingTime.TotalHours).ToString("00") & ":" & (Math.Floor(totalWorkingTime.TotalMinutes) - Math.Floor(totalWorkingTime.TotalHours) * 60).ToString("00")
            dtRow("控除時間") = Math.Floor(totalRestTime.TotalHours).ToString("00") & ":" & (Math.Floor(totalRestTime.TotalMinutes) - Math.Floor(totalRestTime.TotalHours) * 60).ToString("00")
            dttDisplayTable.Rows.Add(dtRow)
        End With

        totalSumTime = totalWorkingTime.Subtract(totalRestTime)
        txtSumWorkingTime.Text = Math.Floor(totalWorkingTime.Subtract(totalRestTime).TotalHours).ToString("00") & ":" & _
                                 (Math.Floor(totalWorkingTime.Subtract(totalRestTime).TotalMinutes) - Math.Floor(totalWorkingTime.Subtract(totalRestTime).TotalHours) * 60).ToString("00")

        Select Math.Floor(totalWorkingTime.Subtract(totalRestTime).TotalHours)

            Case 200 To 240
                MsgBox("厚生労働省が定める過労死ライン（月残業80時間）に近づいています。" & vbCrLf & _
                       "休みたくても休めない状況にある場合は、相談機関に相談してください。")
            Case 240 To 9999
                MsgBox("厚生労働省が定める過労死ライン（月残業80時間）を超えています！！" & vbCrLf & _
                       "今すぐ休暇をとってください。" & vbCrLf & _
                       "休みたくても休めない状況にある場合は、相談機関に相談してください。")

        End Select

        'データグリッドビューにデータテーブルをセット
        dgvDisplay.DataSource = dttDisplayTable

        '色設定
        i = 0
        With dgvDisplay
            For Each Row In .Rows
                If IsNothing(.Rows(i).Cells(0).Value) Then Exit For
                If .Rows(i).Cells(0).Value.ToString.Contains("土") Then .Rows(i).DefaultCellStyle.ForeColor = Color.Blue
                If .Rows(i).Cells(0).Value.ToString.Contains("日") Then .Rows(i).DefaultCellStyle.ForeColor = Color.Red
                i = i + 1
            Next
        End With

    End Sub


End Class
