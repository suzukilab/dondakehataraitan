﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Settings
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.NumericUpDown7 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown8 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown6 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown5 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown4 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown3 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lbl_DEFAULT_WORKING_START = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.NumericUpDown32 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown33 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown34 = New System.Windows.Forms.NumericUpDown()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.NumericUpDown14 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown15 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown16 = New System.Windows.Forms.NumericUpDown()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.TextBox20 = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.NumericUpDown9 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown10 = New System.Windows.Forms.NumericUpDown()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.NumericUpDown11 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown12 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown13 = New System.Windows.Forms.NumericUpDown()
        Me.ListBox3 = New System.Windows.Forms.ListBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.NumericUpDown20 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown19 = New System.Windows.Forms.NumericUpDown()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.NumericUpDown18 = New System.Windows.Forms.NumericUpDown()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.NumericUpDown17 = New System.Windows.Forms.NumericUpDown()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.NumericUpDown37 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown38 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown39 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown40 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown41 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown42 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown43 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown44 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown45 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown46 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown47 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown48 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown49 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown50 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown51 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown52 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown53 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown54 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown55 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown56 = New System.Windows.Forms.NumericUpDown()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.MonthCalendar2 = New System.Windows.Forms.MonthCalendar()
        Me.TabControl.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.NumericUpDown32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown34, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.NumericUpDown14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        CType(Me.NumericUpDown20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage6.SuspendLayout()
        CType(Me.NumericUpDown37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown56, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl
        '
        Me.TabControl.Controls.Add(Me.TabPage1)
        Me.TabControl.Controls.Add(Me.TabPage2)
        Me.TabControl.Controls.Add(Me.TabPage3)
        Me.TabControl.Controls.Add(Me.TabPage4)
        Me.TabControl.Controls.Add(Me.TabPage5)
        Me.TabControl.Controls.Add(Me.TabPage6)
        Me.TabControl.Font = New System.Drawing.Font("メイリオ", 11.25!)
        Me.TabControl.Location = New System.Drawing.Point(12, 12)
        Me.TabControl.Name = "TabControl"
        Me.TabControl.SelectedIndex = 0
        Me.TabControl.Size = New System.Drawing.Size(587, 313)
        Me.TabControl.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.CheckBox2)
        Me.TabPage1.Controls.Add(Me.CheckBox1)
        Me.TabPage1.Controls.Add(Me.NumericUpDown7)
        Me.TabPage1.Controls.Add(Me.NumericUpDown8)
        Me.TabPage1.Controls.Add(Me.NumericUpDown6)
        Me.TabPage1.Controls.Add(Me.NumericUpDown5)
        Me.TabPage1.Controls.Add(Me.NumericUpDown4)
        Me.TabPage1.Controls.Add(Me.NumericUpDown2)
        Me.TabPage1.Controls.Add(Me.NumericUpDown3)
        Me.TabPage1.Controls.Add(Me.NumericUpDown1)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.lbl_DEFAULT_WORKING_START)
        Me.TabPage1.Font = New System.Drawing.Font("メイリオ", 11.25!)
        Me.TabPage1.Location = New System.Drawing.Point(4, 32)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(579, 277)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "勤怠設定"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(211, 64)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(99, 22)
        Me.CheckBox2.TabIndex = 3
        Me.CheckBox2.Text = "一括設定する"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(210, 23)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(99, 22)
        Me.CheckBox1.TabIndex = 3
        Me.CheckBox1.Text = "一括設定する"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'NumericUpDown7
        '
        Me.NumericUpDown7.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown7.Location = New System.Drawing.Point(208, 194)
        Me.NumericUpDown7.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.NumericUpDown7.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown7.Name = "NumericUpDown7"
        Me.NumericUpDown7.Size = New System.Drawing.Size(42, 27)
        Me.NumericUpDown7.TabIndex = 2
        Me.NumericUpDown7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown7.Value = New Decimal(New Integer() {25, 0, 0, 0})
        '
        'NumericUpDown8
        '
        Me.NumericUpDown8.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown8.Location = New System.Drawing.Point(176, 235)
        Me.NumericUpDown8.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumericUpDown8.Name = "NumericUpDown8"
        Me.NumericUpDown8.Size = New System.Drawing.Size(42, 27)
        Me.NumericUpDown8.TabIndex = 2
        Me.NumericUpDown8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown6
        '
        Me.NumericUpDown6.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown6.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown6.Location = New System.Drawing.Point(102, 150)
        Me.NumericUpDown6.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.NumericUpDown6.Name = "NumericUpDown6"
        Me.NumericUpDown6.Size = New System.Drawing.Size(42, 27)
        Me.NumericUpDown6.TabIndex = 2
        Me.NumericUpDown6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown5
        '
        Me.NumericUpDown5.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown5.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown5.Location = New System.Drawing.Point(176, 107)
        Me.NumericUpDown5.Maximum = New Decimal(New Integer() {300, 0, 0, 0})
        Me.NumericUpDown5.Name = "NumericUpDown5"
        Me.NumericUpDown5.Size = New System.Drawing.Size(42, 27)
        Me.NumericUpDown5.TabIndex = 2
        Me.NumericUpDown5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown4
        '
        Me.NumericUpDown4.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown4.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown4.Location = New System.Drawing.Point(150, 62)
        Me.NumericUpDown4.Maximum = New Decimal(New Integer() {55, 0, 0, 0})
        Me.NumericUpDown4.Name = "NumericUpDown4"
        Me.NumericUpDown4.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown4.TabIndex = 2
        Me.NumericUpDown4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown2
        '
        Me.NumericUpDown2.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown2.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown2.Location = New System.Drawing.Point(149, 21)
        Me.NumericUpDown2.Maximum = New Decimal(New Integer() {55, 0, 0, 0})
        Me.NumericUpDown2.Name = "NumericUpDown2"
        Me.NumericUpDown2.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown2.TabIndex = 2
        Me.NumericUpDown2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown3
        '
        Me.NumericUpDown3.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown3.Location = New System.Drawing.Point(95, 62)
        Me.NumericUpDown3.Maximum = New Decimal(New Integer() {23, 0, 0, 0})
        Me.NumericUpDown3.Name = "NumericUpDown3"
        Me.NumericUpDown3.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown3.TabIndex = 1
        Me.NumericUpDown3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown1.Location = New System.Drawing.Point(94, 21)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {23, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown1.TabIndex = 1
        Me.NumericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label1.Location = New System.Drawing.Point(22, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(135, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "終業時間          ："
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label5.Location = New System.Drawing.Point(21, 239)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(220, 23)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "日付境界時間　　　　　　　時"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label4.Location = New System.Drawing.Point(22, 198)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(250, 23)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "給与計算締め日　　　毎月　　　日"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.Location = New System.Drawing.Point(22, 154)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(235, 23)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "勤怠時間を　　　分ごとに丸める"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.Location = New System.Drawing.Point(21, 111)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(235, 23)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "月間所定時間外労働　　　　時間"
        '
        'lbl_DEFAULT_WORKING_START
        '
        Me.lbl_DEFAULT_WORKING_START.AutoSize = True
        Me.lbl_DEFAULT_WORKING_START.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lbl_DEFAULT_WORKING_START.Location = New System.Drawing.Point(21, 23)
        Me.lbl_DEFAULT_WORKING_START.Name = "lbl_DEFAULT_WORKING_START"
        Me.lbl_DEFAULT_WORKING_START.Size = New System.Drawing.Size(135, 23)
        Me.lbl_DEFAULT_WORKING_START.TabIndex = 0
        Me.lbl_DEFAULT_WORKING_START.Text = "始業時間          ："
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.NumericUpDown32)
        Me.TabPage2.Controls.Add(Me.NumericUpDown33)
        Me.TabPage2.Controls.Add(Me.NumericUpDown34)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Controls.Add(Me.TextBox2)
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.TextBox8)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.TextBox10)
        Me.TabPage2.Controls.Add(Me.Label25)
        Me.TabPage2.Controls.Add(Me.TextBox9)
        Me.TabPage2.Controls.Add(Me.Label24)
        Me.TabPage2.Controls.Add(Me.TextBox3)
        Me.TabPage2.Controls.Add(Me.Label14)
        Me.TabPage2.Controls.Add(Me.TextBox11)
        Me.TabPage2.Controls.Add(Me.Label26)
        Me.TabPage2.Controls.Add(Me.TextBox1)
        Me.TabPage2.Controls.Add(Me.Label12)
        Me.TabPage2.Location = New System.Drawing.Point(4, 32)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(579, 277)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "所属会社"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'NumericUpDown32
        '
        Me.NumericUpDown32.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown32.Location = New System.Drawing.Point(180, 172)
        Me.NumericUpDown32.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumericUpDown32.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown32.Name = "NumericUpDown32"
        Me.NumericUpDown32.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown32.TabIndex = 52
        Me.NumericUpDown32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown32.Value = New Decimal(New Integer() {12, 0, 0, 0})
        '
        'NumericUpDown33
        '
        Me.NumericUpDown33.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown33.Location = New System.Drawing.Point(241, 172)
        Me.NumericUpDown33.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.NumericUpDown33.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown33.Name = "NumericUpDown33"
        Me.NumericUpDown33.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown33.TabIndex = 51
        Me.NumericUpDown33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown33.Value = New Decimal(New Integer() {31, 0, 0, 0})
        '
        'NumericUpDown34
        '
        Me.NumericUpDown34.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown34.Location = New System.Drawing.Point(100, 172)
        Me.NumericUpDown34.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
        Me.NumericUpDown34.Minimum = New Decimal(New Integer() {1915, 0, 0, 0})
        Me.NumericUpDown34.Name = "NumericUpDown34"
        Me.NumericUpDown34.Size = New System.Drawing.Size(59, 27)
        Me.NumericUpDown34.TabIndex = 50
        Me.NumericUpDown34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown34.Value = New Decimal(New Integer() {1990, 0, 0, 0})
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label23.Location = New System.Drawing.Point(27, 176)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(275, 23)
        Me.Label23.TabIndex = 49
        Me.Label23.Text = "入社日　  　　　　年　　　月　　　日"
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(100, 79)
        Me.TextBox2.MaxLength = 7
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(70, 25)
        Me.TextBox2.TabIndex = 46
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label13.Location = New System.Drawing.Point(27, 81)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(70, 23)
        Me.Label13.TabIndex = 36
        Me.Label13.Text = "郵便番号"
        '
        'TextBox8
        '
        Me.TextBox8.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox8.Location = New System.Drawing.Point(100, 141)
        Me.TextBox8.MaxLength = 12
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(152, 25)
        Me.TextBox8.TabIndex = 47
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label22.Location = New System.Drawing.Point(27, 143)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(70, 23)
        Me.Label22.TabIndex = 35
        Me.Label22.Text = "電話番号"
        '
        'TextBox10
        '
        Me.TextBox10.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox10.Location = New System.Drawing.Point(100, 236)
        Me.TextBox10.MaxLength = 12
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(452, 25)
        Me.TextBox10.TabIndex = 48
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label25.Location = New System.Drawing.Point(27, 238)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(40, 23)
        Me.Label25.TabIndex = 37
        Me.Label25.Text = "役職"
        '
        'TextBox9
        '
        Me.TextBox9.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox9.Location = New System.Drawing.Point(100, 205)
        Me.TextBox9.MaxLength = 12
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(452, 25)
        Me.TextBox9.TabIndex = 45
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label24.Location = New System.Drawing.Point(27, 207)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(70, 23)
        Me.Label24.TabIndex = 38
        Me.Label24.Text = "所属部署"
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(100, 110)
        Me.TextBox3.MaxLength = 12
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(452, 25)
        Me.TextBox3.TabIndex = 44
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label14.Location = New System.Drawing.Point(27, 112)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(40, 23)
        Me.Label14.TabIndex = 39
        Me.Label14.Text = "住所"
        '
        'TextBox11
        '
        Me.TextBox11.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox11.Location = New System.Drawing.Point(100, 48)
        Me.TextBox11.MaxLength = 50
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(452, 25)
        Me.TextBox11.TabIndex = 42
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label26.Location = New System.Drawing.Point(27, 50)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(55, 23)
        Me.Label26.TabIndex = 41
        Me.Label26.Text = "支店名"
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(100, 16)
        Me.TextBox1.MaxLength = 50
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(452, 25)
        Me.TextBox1.TabIndex = 43
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label12.Location = New System.Drawing.Point(27, 18)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(55, 23)
        Me.Label12.TabIndex = 40
        Me.Label12.Text = "会社名"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.CheckBox3)
        Me.TabPage3.Controls.Add(Me.NumericUpDown14)
        Me.TabPage3.Controls.Add(Me.NumericUpDown15)
        Me.TabPage3.Controls.Add(Me.NumericUpDown16)
        Me.TabPage3.Controls.Add(Me.Label15)
        Me.TabPage3.Controls.Add(Me.TextBox4)
        Me.TabPage3.Controls.Add(Me.Label16)
        Me.TabPage3.Controls.Add(Me.TextBox6)
        Me.TabPage3.Controls.Add(Me.Label17)
        Me.TabPage3.Controls.Add(Me.TextBox12)
        Me.TabPage3.Controls.Add(Me.Label20)
        Me.TabPage3.Controls.Add(Me.TextBox13)
        Me.TabPage3.Controls.Add(Me.Label21)
        Me.TabPage3.Controls.Add(Me.TextBox14)
        Me.TabPage3.Controls.Add(Me.Label27)
        Me.TabPage3.Controls.Add(Me.TextBox20)
        Me.TabPage3.Controls.Add(Me.Label28)
        Me.TabPage3.Location = New System.Drawing.Point(4, 32)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(579, 277)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "勤務先"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.CheckBox3.Location = New System.Drawing.Point(28, 19)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(111, 22)
        Me.CheckBox3.TabIndex = 53
        Me.CheckBox3.Text = "所属会社と同じ"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'NumericUpDown14
        '
        Me.NumericUpDown14.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown14.Location = New System.Drawing.Point(207, 203)
        Me.NumericUpDown14.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumericUpDown14.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown14.Name = "NumericUpDown14"
        Me.NumericUpDown14.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown14.TabIndex = 52
        Me.NumericUpDown14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown14.Value = New Decimal(New Integer() {12, 0, 0, 0})
        '
        'NumericUpDown15
        '
        Me.NumericUpDown15.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown15.Location = New System.Drawing.Point(268, 203)
        Me.NumericUpDown15.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.NumericUpDown15.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown15.Name = "NumericUpDown15"
        Me.NumericUpDown15.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown15.TabIndex = 51
        Me.NumericUpDown15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown15.Value = New Decimal(New Integer() {31, 0, 0, 0})
        '
        'NumericUpDown16
        '
        Me.NumericUpDown16.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown16.Location = New System.Drawing.Point(127, 203)
        Me.NumericUpDown16.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
        Me.NumericUpDown16.Minimum = New Decimal(New Integer() {1915, 0, 0, 0})
        Me.NumericUpDown16.Name = "NumericUpDown16"
        Me.NumericUpDown16.Size = New System.Drawing.Size(59, 27)
        Me.NumericUpDown16.TabIndex = 50
        Me.NumericUpDown16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown16.Value = New Decimal(New Integer() {1990, 0, 0, 0})
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label15.Location = New System.Drawing.Point(24, 207)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(305, 23)
        Me.Label15.TabIndex = 49
        Me.Label15.Text = "勤務開始日　  　　　　年　　　月　　　日"
        '
        'TextBox4
        '
        Me.TextBox4.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(97, 110)
        Me.TextBox4.MaxLength = 7
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(70, 25)
        Me.TextBox4.TabIndex = 46
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label16.Location = New System.Drawing.Point(24, 112)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(70, 23)
        Me.Label16.TabIndex = 36
        Me.Label16.Text = "郵便番号"
        '
        'TextBox6
        '
        Me.TextBox6.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox6.Location = New System.Drawing.Point(97, 172)
        Me.TextBox6.MaxLength = 12
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(152, 25)
        Me.TextBox6.TabIndex = 47
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label17.Location = New System.Drawing.Point(24, 174)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(70, 23)
        Me.Label17.TabIndex = 35
        Me.Label17.Text = "電話番号"
        '
        'TextBox12
        '
        Me.TextBox12.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox12.Location = New System.Drawing.Point(97, 236)
        Me.TextBox12.MaxLength = 12
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(452, 25)
        Me.TextBox12.TabIndex = 45
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label20.Location = New System.Drawing.Point(24, 238)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(70, 23)
        Me.Label20.TabIndex = 38
        Me.Label20.Text = "所属部署"
        '
        'TextBox13
        '
        Me.TextBox13.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox13.Location = New System.Drawing.Point(97, 141)
        Me.TextBox13.MaxLength = 12
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(452, 25)
        Me.TextBox13.TabIndex = 44
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label21.Location = New System.Drawing.Point(24, 143)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(40, 23)
        Me.Label21.TabIndex = 39
        Me.Label21.Text = "住所"
        '
        'TextBox14
        '
        Me.TextBox14.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox14.Location = New System.Drawing.Point(97, 79)
        Me.TextBox14.MaxLength = 50
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(452, 25)
        Me.TextBox14.TabIndex = 42
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label27.Location = New System.Drawing.Point(24, 81)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(55, 23)
        Me.Label27.TabIndex = 41
        Me.Label27.Text = "支店名"
        '
        'TextBox20
        '
        Me.TextBox20.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox20.Location = New System.Drawing.Point(97, 47)
        Me.TextBox20.MaxLength = 50
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.Size = New System.Drawing.Size(452, 25)
        Me.TextBox20.TabIndex = 43
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label28.Location = New System.Drawing.Point(24, 49)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(55, 23)
        Me.Label28.TabIndex = 40
        Me.Label28.Text = "会社名"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.NumericUpDown9)
        Me.TabPage4.Controls.Add(Me.NumericUpDown10)
        Me.TabPage4.Controls.Add(Me.Label6)
        Me.TabPage4.Controls.Add(Me.TextBox5)
        Me.TabPage4.Controls.Add(Me.Label7)
        Me.TabPage4.Controls.Add(Me.TextBox15)
        Me.TabPage4.Controls.Add(Me.Label8)
        Me.TabPage4.Controls.Add(Me.TextBox16)
        Me.TabPage4.Controls.Add(Me.TextBox17)
        Me.TabPage4.Controls.Add(Me.Label9)
        Me.TabPage4.Controls.Add(Me.Label10)
        Me.TabPage4.Controls.Add(Me.NumericUpDown11)
        Me.TabPage4.Controls.Add(Me.NumericUpDown12)
        Me.TabPage4.Controls.Add(Me.NumericUpDown13)
        Me.TabPage4.Controls.Add(Me.ListBox3)
        Me.TabPage4.Controls.Add(Me.Label11)
        Me.TabPage4.Controls.Add(Me.TextBox18)
        Me.TabPage4.Controls.Add(Me.Label18)
        Me.TabPage4.Controls.Add(Me.TextBox19)
        Me.TabPage4.Controls.Add(Me.Label38)
        Me.TabPage4.Controls.Add(Me.Label39)
        Me.TabPage4.Location = New System.Drawing.Point(4, 32)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(579, 277)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "個人設定"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'NumericUpDown9
        '
        Me.NumericUpDown9.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown9.Location = New System.Drawing.Point(191, 234)
        Me.NumericUpDown9.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumericUpDown9.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown9.Name = "NumericUpDown9"
        Me.NumericUpDown9.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown9.TabIndex = 51
        Me.NumericUpDown9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown9.Value = New Decimal(New Integer() {12, 0, 0, 0})
        '
        'NumericUpDown10
        '
        Me.NumericUpDown10.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown10.Location = New System.Drawing.Point(111, 234)
        Me.NumericUpDown10.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
        Me.NumericUpDown10.Minimum = New Decimal(New Integer() {1915, 0, 0, 0})
        Me.NumericUpDown10.Name = "NumericUpDown10"
        Me.NumericUpDown10.Size = New System.Drawing.Size(59, 27)
        Me.NumericUpDown10.TabIndex = 48
        Me.NumericUpDown10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown10.Value = New Decimal(New Integer() {1990, 0, 0, 0})
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label6.Location = New System.Drawing.Point(27, 238)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(360, 23)
        Me.Label6.TabIndex = 59
        Me.Label6.Text = "この仕事を　 　　　年　　　月にスタートしました"
        '
        'TextBox5
        '
        Me.TextBox5.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(100, 203)
        Me.TextBox5.MaxLength = 12
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(452, 25)
        Me.TextBox5.TabIndex = 60
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label7.Location = New System.Drawing.Point(27, 205)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 23)
        Me.Label7.TabIndex = 58
        Me.Label7.Text = "職種"
        '
        'TextBox15
        '
        Me.TextBox15.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox15.Location = New System.Drawing.Point(100, 141)
        Me.TextBox15.MaxLength = 12
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(152, 25)
        Me.TextBox15.TabIndex = 57
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label8.Location = New System.Drawing.Point(27, 143)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(70, 23)
        Me.Label8.TabIndex = 55
        Me.Label8.Text = "電話番号"
        '
        'TextBox16
        '
        Me.TextBox16.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox16.Location = New System.Drawing.Point(100, 110)
        Me.TextBox16.MaxLength = 12
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New System.Drawing.Size(452, 25)
        Me.TextBox16.TabIndex = 56
        '
        'TextBox17
        '
        Me.TextBox17.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox17.Location = New System.Drawing.Point(100, 79)
        Me.TextBox17.MaxLength = 7
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(70, 25)
        Me.TextBox17.TabIndex = 54
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label9.Location = New System.Drawing.Point(27, 110)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 23)
        Me.Label9.TabIndex = 53
        Me.Label9.Text = "住所"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label10.Location = New System.Drawing.Point(27, 81)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(70, 23)
        Me.Label10.TabIndex = 52
        Me.Label10.Text = "郵便番号"
        '
        'NumericUpDown11
        '
        Me.NumericUpDown11.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown11.Location = New System.Drawing.Point(180, 47)
        Me.NumericUpDown11.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumericUpDown11.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown11.Name = "NumericUpDown11"
        Me.NumericUpDown11.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown11.TabIndex = 50
        Me.NumericUpDown11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown11.Value = New Decimal(New Integer() {12, 0, 0, 0})
        '
        'NumericUpDown12
        '
        Me.NumericUpDown12.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown12.Location = New System.Drawing.Point(241, 47)
        Me.NumericUpDown12.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.NumericUpDown12.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown12.Name = "NumericUpDown12"
        Me.NumericUpDown12.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown12.TabIndex = 49
        Me.NumericUpDown12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown12.Value = New Decimal(New Integer() {31, 0, 0, 0})
        '
        'NumericUpDown13
        '
        Me.NumericUpDown13.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown13.Location = New System.Drawing.Point(100, 47)
        Me.NumericUpDown13.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
        Me.NumericUpDown13.Minimum = New Decimal(New Integer() {1915, 0, 0, 0})
        Me.NumericUpDown13.Name = "NumericUpDown13"
        Me.NumericUpDown13.Size = New System.Drawing.Size(59, 27)
        Me.NumericUpDown13.TabIndex = 47
        Me.NumericUpDown13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown13.Value = New Decimal(New Integer() {1990, 0, 0, 0})
        '
        'ListBox3
        '
        Me.ListBox3.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.ListBox3.FormattingEnabled = True
        Me.ListBox3.ItemHeight = 18
        Me.ListBox3.Items.AddRange(New Object() {"男", "女"})
        Me.ListBox3.Location = New System.Drawing.Point(497, 19)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(48, 22)
        Me.ListBox3.TabIndex = 46
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label11.Location = New System.Drawing.Point(27, 51)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(275, 23)
        Me.Label11.TabIndex = 41
        Me.Label11.Text = "誕生日　  　　　　年　　　月　　　日"
        '
        'TextBox18
        '
        Me.TextBox18.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox18.Location = New System.Drawing.Point(100, 172)
        Me.TextBox18.MaxLength = 12
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New System.Drawing.Size(388, 25)
        Me.TextBox18.TabIndex = 45
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label18.Location = New System.Drawing.Point(27, 174)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(70, 23)
        Me.Label18.TabIndex = 40
        Me.Label18.Text = "Ｅメール"
        '
        'TextBox19
        '
        Me.TextBox19.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.TextBox19.Location = New System.Drawing.Point(100, 16)
        Me.TextBox19.MaxLength = 50
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Size = New System.Drawing.Size(329, 25)
        Me.TextBox19.TabIndex = 44
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label38.Location = New System.Drawing.Point(450, 19)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(40, 23)
        Me.Label38.TabIndex = 43
        Me.Label38.Text = "性別"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label39.Location = New System.Drawing.Point(27, 18)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(40, 23)
        Me.Label39.TabIndex = 42
        Me.Label39.Text = "氏名"
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.NumericUpDown20)
        Me.TabPage5.Controls.Add(Me.NumericUpDown19)
        Me.TabPage5.Controls.Add(Me.Label42)
        Me.TabPage5.Controls.Add(Me.NumericUpDown18)
        Me.TabPage5.Controls.Add(Me.Label41)
        Me.TabPage5.Controls.Add(Me.NumericUpDown17)
        Me.TabPage5.Controls.Add(Me.Label40)
        Me.TabPage5.Controls.Add(Me.Label29)
        Me.TabPage5.Controls.Add(Me.Label19)
        Me.TabPage5.Controls.Add(Me.Label30)
        Me.TabPage5.Controls.Add(Me.ListBox1)
        Me.TabPage5.Controls.Add(Me.ListBox2)
        Me.TabPage5.Location = New System.Drawing.Point(4, 32)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(579, 277)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "契約条件"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'NumericUpDown20
        '
        Me.NumericUpDown20.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown20.Location = New System.Drawing.Point(141, 200)
        Me.NumericUpDown20.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.NumericUpDown20.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown20.Name = "NumericUpDown20"
        Me.NumericUpDown20.Size = New System.Drawing.Size(96, 27)
        Me.NumericUpDown20.TabIndex = 48
        Me.NumericUpDown20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown20.Value = New Decimal(New Integer() {1990, 0, 0, 0})
        '
        'NumericUpDown19
        '
        Me.NumericUpDown19.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown19.Location = New System.Drawing.Point(141, 165)
        Me.NumericUpDown19.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.NumericUpDown19.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown19.Name = "NumericUpDown19"
        Me.NumericUpDown19.Size = New System.Drawing.Size(96, 27)
        Me.NumericUpDown19.TabIndex = 48
        Me.NumericUpDown19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown19.Value = New Decimal(New Integer() {1990, 0, 0, 0})
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label42.Location = New System.Drawing.Point(19, 204)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(240, 23)
        Me.Label42.TabIndex = 11
        Me.Label42.Text = "固定残業代　 　　　　　　　　円"
        '
        'NumericUpDown18
        '
        Me.NumericUpDown18.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown18.Location = New System.Drawing.Point(141, 130)
        Me.NumericUpDown18.Maximum = New Decimal(New Integer() {99999999, 0, 0, 0})
        Me.NumericUpDown18.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown18.Name = "NumericUpDown18"
        Me.NumericUpDown18.Size = New System.Drawing.Size(96, 27)
        Me.NumericUpDown18.TabIndex = 48
        Me.NumericUpDown18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown18.Value = New Decimal(New Integer() {1990, 0, 0, 0})
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label41.Location = New System.Drawing.Point(19, 169)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(240, 23)
        Me.Label41.TabIndex = 11
        Me.Label41.Text = "残業時給 　　　　　　　　　　円"
        '
        'NumericUpDown17
        '
        Me.NumericUpDown17.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown17.Location = New System.Drawing.Point(141, 93)
        Me.NumericUpDown17.Maximum = New Decimal(New Integer() {99999999, 0, 0, 0})
        Me.NumericUpDown17.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown17.Name = "NumericUpDown17"
        Me.NumericUpDown17.Size = New System.Drawing.Size(96, 27)
        Me.NumericUpDown17.TabIndex = 48
        Me.NumericUpDown17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown17.Value = New Decimal(New Integer() {150000, 0, 0, 0})
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label40.Location = New System.Drawing.Point(19, 134)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(240, 23)
        Me.Label40.TabIndex = 11
        Me.Label40.Text = "各種手当 　　　　　　　　　　円"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label29.Location = New System.Drawing.Point(19, 97)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(240, 23)
        Me.Label29.TabIndex = 11
        Me.Label29.Text = "給与 　　　　　　　　　　　　円"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label19.Location = New System.Drawing.Point(19, 58)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(70, 23)
        Me.Label19.TabIndex = 11
        Me.Label19.Text = "給与形態"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label30.Location = New System.Drawing.Point(19, 20)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(70, 23)
        Me.Label30.TabIndex = 11
        Me.Label30.Text = "契約形態"
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 18
        Me.ListBox1.Items.AddRange(New Object() {"月給", "時給", "日給", "年俸"})
        Me.ListBox1.Location = New System.Drawing.Point(141, 58)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(96, 22)
        Me.ListBox1.TabIndex = 10
        '
        'ListBox2
        '
        Me.ListBox2.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 18
        Me.ListBox2.Items.AddRange(New Object() {"正社員・職員", "パート・アルバイト", "契約社員", "派遣社員", "嘱託社員", "臨時・日雇労働者", "役員・個人事業主", "その他の非正規社員・職員"})
        Me.ListBox2.Location = New System.Drawing.Point(141, 20)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(96, 22)
        Me.ListBox2.TabIndex = 10
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.NumericUpDown37)
        Me.TabPage6.Controls.Add(Me.NumericUpDown38)
        Me.TabPage6.Controls.Add(Me.NumericUpDown39)
        Me.TabPage6.Controls.Add(Me.NumericUpDown40)
        Me.TabPage6.Controls.Add(Me.NumericUpDown41)
        Me.TabPage6.Controls.Add(Me.NumericUpDown42)
        Me.TabPage6.Controls.Add(Me.NumericUpDown43)
        Me.TabPage6.Controls.Add(Me.NumericUpDown44)
        Me.TabPage6.Controls.Add(Me.NumericUpDown45)
        Me.TabPage6.Controls.Add(Me.NumericUpDown46)
        Me.TabPage6.Controls.Add(Me.NumericUpDown47)
        Me.TabPage6.Controls.Add(Me.NumericUpDown48)
        Me.TabPage6.Controls.Add(Me.NumericUpDown49)
        Me.TabPage6.Controls.Add(Me.NumericUpDown50)
        Me.TabPage6.Controls.Add(Me.NumericUpDown51)
        Me.TabPage6.Controls.Add(Me.NumericUpDown52)
        Me.TabPage6.Controls.Add(Me.NumericUpDown53)
        Me.TabPage6.Controls.Add(Me.NumericUpDown54)
        Me.TabPage6.Controls.Add(Me.NumericUpDown55)
        Me.TabPage6.Controls.Add(Me.NumericUpDown56)
        Me.TabPage6.Controls.Add(Me.Label31)
        Me.TabPage6.Controls.Add(Me.Label32)
        Me.TabPage6.Controls.Add(Me.Label33)
        Me.TabPage6.Controls.Add(Me.Label34)
        Me.TabPage6.Controls.Add(Me.Label35)
        Me.TabPage6.Controls.Add(Me.Label36)
        Me.TabPage6.Controls.Add(Me.Label37)
        Me.TabPage6.Controls.Add(Me.MonthCalendar2)
        Me.TabPage6.Location = New System.Drawing.Point(4, 32)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(579, 277)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "休日・休憩"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'NumericUpDown37
        '
        Me.NumericUpDown37.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown37.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown37.Location = New System.Drawing.Point(521, 213)
        Me.NumericUpDown37.Maximum = New Decimal(New Integer() {55, 0, 0, 0})
        Me.NumericUpDown37.Name = "NumericUpDown37"
        Me.NumericUpDown37.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown37.TabIndex = 52
        Me.NumericUpDown37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown38
        '
        Me.NumericUpDown38.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown38.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown38.Location = New System.Drawing.Point(402, 213)
        Me.NumericUpDown38.Maximum = New Decimal(New Integer() {55, 0, 0, 0})
        Me.NumericUpDown38.Name = "NumericUpDown38"
        Me.NumericUpDown38.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown38.TabIndex = 53
        Me.NumericUpDown38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown39
        '
        Me.NumericUpDown39.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown39.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown39.Location = New System.Drawing.Point(521, 176)
        Me.NumericUpDown39.Maximum = New Decimal(New Integer() {55, 0, 0, 0})
        Me.NumericUpDown39.Name = "NumericUpDown39"
        Me.NumericUpDown39.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown39.TabIndex = 51
        Me.NumericUpDown39.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown40
        '
        Me.NumericUpDown40.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown40.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown40.Location = New System.Drawing.Point(521, 138)
        Me.NumericUpDown40.Maximum = New Decimal(New Integer() {55, 0, 0, 0})
        Me.NumericUpDown40.Name = "NumericUpDown40"
        Me.NumericUpDown40.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown40.TabIndex = 49
        Me.NumericUpDown40.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown41
        '
        Me.NumericUpDown41.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown41.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown41.Location = New System.Drawing.Point(402, 176)
        Me.NumericUpDown41.Maximum = New Decimal(New Integer() {55, 0, 0, 0})
        Me.NumericUpDown41.Name = "NumericUpDown41"
        Me.NumericUpDown41.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown41.TabIndex = 50
        Me.NumericUpDown41.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown42
        '
        Me.NumericUpDown42.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown42.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown42.Location = New System.Drawing.Point(521, 101)
        Me.NumericUpDown42.Maximum = New Decimal(New Integer() {55, 0, 0, 0})
        Me.NumericUpDown42.Name = "NumericUpDown42"
        Me.NumericUpDown42.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown42.TabIndex = 57
        Me.NumericUpDown42.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown43
        '
        Me.NumericUpDown43.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown43.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown43.Location = New System.Drawing.Point(402, 138)
        Me.NumericUpDown43.Maximum = New Decimal(New Integer() {55, 0, 0, 0})
        Me.NumericUpDown43.Name = "NumericUpDown43"
        Me.NumericUpDown43.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown43.TabIndex = 58
        Me.NumericUpDown43.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown44
        '
        Me.NumericUpDown44.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown44.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown44.Location = New System.Drawing.Point(521, 64)
        Me.NumericUpDown44.Maximum = New Decimal(New Integer() {55, 0, 0, 0})
        Me.NumericUpDown44.Name = "NumericUpDown44"
        Me.NumericUpDown44.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown44.TabIndex = 56
        Me.NumericUpDown44.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown45
        '
        Me.NumericUpDown45.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown45.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown45.Location = New System.Drawing.Point(402, 101)
        Me.NumericUpDown45.Maximum = New Decimal(New Integer() {55, 0, 0, 0})
        Me.NumericUpDown45.Name = "NumericUpDown45"
        Me.NumericUpDown45.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown45.TabIndex = 54
        Me.NumericUpDown45.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown46
        '
        Me.NumericUpDown46.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown46.Location = New System.Drawing.Point(470, 213)
        Me.NumericUpDown46.Maximum = New Decimal(New Integer() {23, 0, 0, 0})
        Me.NumericUpDown46.Name = "NumericUpDown46"
        Me.NumericUpDown46.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown46.TabIndex = 47
        Me.NumericUpDown46.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown47
        '
        Me.NumericUpDown47.Font = New System.Drawing.Font("メイリオ", 9.75!)
        Me.NumericUpDown47.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown47.Location = New System.Drawing.Point(402, 64)
        Me.NumericUpDown47.Maximum = New Decimal(New Integer() {55, 0, 0, 0})
        Me.NumericUpDown47.Name = "NumericUpDown47"
        Me.NumericUpDown47.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown47.TabIndex = 55
        Me.NumericUpDown47.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown48
        '
        Me.NumericUpDown48.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown48.Location = New System.Drawing.Point(470, 176)
        Me.NumericUpDown48.Maximum = New Decimal(New Integer() {23, 0, 0, 0})
        Me.NumericUpDown48.Name = "NumericUpDown48"
        Me.NumericUpDown48.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown48.TabIndex = 48
        Me.NumericUpDown48.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown49
        '
        Me.NumericUpDown49.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown49.Location = New System.Drawing.Point(350, 213)
        Me.NumericUpDown49.Maximum = New Decimal(New Integer() {23, 0, 0, 0})
        Me.NumericUpDown49.Name = "NumericUpDown49"
        Me.NumericUpDown49.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown49.TabIndex = 46
        Me.NumericUpDown49.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown50
        '
        Me.NumericUpDown50.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown50.Location = New System.Drawing.Point(470, 138)
        Me.NumericUpDown50.Maximum = New Decimal(New Integer() {23, 0, 0, 0})
        Me.NumericUpDown50.Name = "NumericUpDown50"
        Me.NumericUpDown50.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown50.TabIndex = 41
        Me.NumericUpDown50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown51
        '
        Me.NumericUpDown51.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown51.Location = New System.Drawing.Point(350, 176)
        Me.NumericUpDown51.Maximum = New Decimal(New Integer() {23, 0, 0, 0})
        Me.NumericUpDown51.Name = "NumericUpDown51"
        Me.NumericUpDown51.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown51.TabIndex = 40
        Me.NumericUpDown51.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown52
        '
        Me.NumericUpDown52.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown52.Location = New System.Drawing.Point(470, 101)
        Me.NumericUpDown52.Maximum = New Decimal(New Integer() {23, 0, 0, 0})
        Me.NumericUpDown52.Name = "NumericUpDown52"
        Me.NumericUpDown52.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown52.TabIndex = 39
        Me.NumericUpDown52.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown53
        '
        Me.NumericUpDown53.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown53.Location = New System.Drawing.Point(350, 138)
        Me.NumericUpDown53.Maximum = New Decimal(New Integer() {23, 0, 0, 0})
        Me.NumericUpDown53.Name = "NumericUpDown53"
        Me.NumericUpDown53.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown53.TabIndex = 42
        Me.NumericUpDown53.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown54
        '
        Me.NumericUpDown54.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown54.Location = New System.Drawing.Point(470, 64)
        Me.NumericUpDown54.Maximum = New Decimal(New Integer() {23, 0, 0, 0})
        Me.NumericUpDown54.Name = "NumericUpDown54"
        Me.NumericUpDown54.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown54.TabIndex = 45
        Me.NumericUpDown54.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown55
        '
        Me.NumericUpDown55.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown55.Location = New System.Drawing.Point(350, 101)
        Me.NumericUpDown55.Maximum = New Decimal(New Integer() {23, 0, 0, 0})
        Me.NumericUpDown55.Name = "NumericUpDown55"
        Me.NumericUpDown55.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown55.TabIndex = 44
        Me.NumericUpDown55.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumericUpDown56
        '
        Me.NumericUpDown56.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.NumericUpDown56.Location = New System.Drawing.Point(350, 64)
        Me.NumericUpDown56.Maximum = New Decimal(New Integer() {23, 0, 0, 0})
        Me.NumericUpDown56.Name = "NumericUpDown56"
        Me.NumericUpDown56.Size = New System.Drawing.Size(40, 27)
        Me.NumericUpDown56.TabIndex = 43
        Me.NumericUpDown56.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label31.Location = New System.Drawing.Point(259, 215)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(270, 23)
        Me.Label31.TabIndex = 35
        Me.Label31.Text = "休憩時間⑤          ：　　　～　　　："
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label32.Location = New System.Drawing.Point(259, 178)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(270, 23)
        Me.Label32.TabIndex = 34
        Me.Label32.Text = "休憩時間④          ：　　　～　　　："
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label33.Location = New System.Drawing.Point(259, 140)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(270, 23)
        Me.Label33.TabIndex = 36
        Me.Label33.Text = "休憩時間③          ：　　　～　　　："
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label34.Location = New System.Drawing.Point(259, 103)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(270, 23)
        Me.Label34.TabIndex = 38
        Me.Label34.Text = "休憩時間②          ：　　　～　　　："
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label35.Location = New System.Drawing.Point(259, 66)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(270, 23)
        Me.Label35.TabIndex = 37
        Me.Label35.Text = "休憩時間①          ：　　　～　　　："
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label36.Location = New System.Drawing.Point(259, 23)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(205, 23)
        Me.Label36.TabIndex = 32
        Me.Label36.Text = "休憩時間を設定してください"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("メイリオ", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label37.Location = New System.Drawing.Point(17, 23)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(175, 23)
        Me.Label37.TabIndex = 33
        Me.Label37.Text = "休日を選択してください"
        '
        'MonthCalendar2
        '
        Me.MonthCalendar2.Location = New System.Drawing.Point(21, 64)
        Me.MonthCalendar2.Name = "MonthCalendar2"
        Me.MonthCalendar2.TabIndex = 31
        '
        'Settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(611, 371)
        Me.Controls.Add(Me.TabControl)
        Me.Name = "Settings"
        Me.Text = "会社設定"
        Me.TabControl.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.NumericUpDown32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown34, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.NumericUpDown14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        CType(Me.NumericUpDown20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        CType(Me.NumericUpDown37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown56, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents lbl_DEFAULT_WORKING_START As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown4 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown3 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents NumericUpDown5 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown6 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown7 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown8 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents NumericUpDown32 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown33 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown34 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown14 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown15 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown16 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents TextBox20 As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown9 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown10 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown11 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown12 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown13 As System.Windows.Forms.NumericUpDown
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents NumericUpDown37 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown38 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown39 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown40 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown41 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown42 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown43 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown44 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown45 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown46 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown47 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown48 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown49 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown50 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown51 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown52 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown53 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown54 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown55 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown56 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents MonthCalendar2 As System.Windows.Forms.MonthCalendar
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents NumericUpDown18 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown17 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown19 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown20 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label42 As System.Windows.Forms.Label
End Class
